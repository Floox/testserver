import asyncio
import base64
import time
import json
from cryptography import fernet
from multidict import MultiDict
from aiohttp import web
import peewee
import peewee_async
from aiohttp_session import setup, get_session
from aiohttp_session.cookie_storage import EncryptedCookieStorage


database = peewee_async.PooledPostgresqlDatabase(None, max_connections=20)

class BaseModel(peewee.Model):


    class Meta:
        database = database

class User(BaseModel):

    login = peewee.CharField()
    password = peewee.CharField()
    counter = peewee.IntegerField(default=0)

DATABASE = {
    'database': 'postgres',
    'password': 'postgres',
    'user': 'postgres',
    'host': 'localhost',
}

# class for memcached
class mUser:
    def __init__(self, id, name, password, counter):
        self.id = id
        self.name = name
        self.password = password
        self.counter = counter
    def decreaseCounter():
        pass
    def increaseCounter():
        pass

def loginRequired(func):
    async def wrapper(request):
        session = await get_session(request)
        name = session.get("name")
        if not name:
            return web.Response(
                body=json.dumps({"message": "Unauthorized user. Sign in/up."}).encode('utf-8'),
                status=401,
                content_type="text/json")
        return await func(request, name)
    return wrapper

@loginRequired
async def getCount(request, name):
    app = request.app
    return web.Response(text="0")

@loginRequired
async def incrementCounter(request, name):
    pass

@loginRequired
async def decrementCounter(request, name):
    pass

async def doLogin(request):
    app = request.app
    session = await get_session(request)
    data = await request.post()
    mData = MultiDict(data)
    name = mData.get('login')
    password = mData.get('password')
    if (len(mData) != 2 or not (name and password)):
        return web.Response(text="Invalid payload. Expected only login and password")
    else:
        # check if new pair (name, password) is correct
        user = await app.objects.execute(User.select().where(User.login == name))
        if user:
            # TODO: use memcached with class mUser for simple working with user
            userPass = user[0].password
            if userPass != password:
                return web.Response(text="Invalid password")
        else:
            await app.objects.create(User, login=name, password=password)
    # TODO: use token instead of name (token will be a key to get user)
    session['name'] = name
    print(name)
    return web.Response(text='SUCCESS')

def app(loop=asyncio.get_event_loop()):
    myapp = web.Application(loop=loop)
    fernet_key = fernet.Fernet.generate_key()
    secret_key = base64.urlsafe_b64decode(fernet_key)
    setup(myapp, EncryptedCookieStorage(secret_key))
    myapp.router.add_route('GET', '/count', getCount)
    myapp.router.add_route('POST', '/login', doLogin)
    myapp.router.add_route('POST', '/count', incrementCounter)
    myapp.router.add_route('DELETE', '/count', decrementCounter)
    return myapp

async def shutdown(app, server, handler):
    server.close()
    await server.wait_closed()
    await app.shutdown()
    # await handler.finish_connections(1.0)
    await app.cleanup()

if __name__ == '__main__':
    database.init(**DATABASE)
    database.create_tables([User], safe=True)
    loop = asyncio.get_event_loop()
    the_app = app(loop)
    the_app.database = database
    the_app.database.set_allow_sync(False)
    the_app.objects = peewee_async.Manager(the_app.database)
    handler = the_app.make_handler()
    foo = loop.create_server(handler, '127.0.0.1', 8090)
    foo = loop.run_until_complete(foo)
    try:
        loop.run_forever()
    except:
        loop.run_until_complete(shutdown(the_app, foo, handler))
        loop.close()
